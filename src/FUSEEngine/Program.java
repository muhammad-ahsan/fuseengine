/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FUSEEngine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 *
 * @author ahsan
 */
public class Program {

    /**
     * @param args the command line arguments
     */
    // Last index of value String array contains count
    private static HashMap<String, String[]> ResearcherPRF = new HashMap<String, String[]>();
    private static HashMap<String, String[]> ResearcherCRF = new HashMap<String, String[]>();
    private static HashMap<String, String[]> ResearcherLabels = new HashMap<String, String[]>();
    private static HashMap<String, String[]> ResearcherNF = new HashMap<String, String[]>();
    private static HashMap<String, String[]> TargetLabels = new HashMap<String, String[]>();

    private static HashMap<String, String[]> LoadFCSV(String Path, int TotalColms,
            boolean isRelationalFeatures, boolean PrintConsole) throws FileNotFoundException,
            IOException, IllegalAccessException {

        HashMap<String, String[]> ResearcherFeature = new HashMap<String, String[]>();


        BufferedReader reader = new BufferedReader(new FileReader(Path));
        String nextLine = reader.readLine();
        String[] strToken;
        // Last element will take count of values for averages
        int count = 0;
        String Key;
        boolean header = true;
        DecimalFormat df = new DecimalFormat("#.##");
        while (nextLine != null) {
            // Skipping header of file

            if (header == true) {
                header = false;
                nextLine = reader.readLine();
                continue;
            }
            String[] Value;
            if (isRelationalFeatures == true) {
                // Last 1 is for select count(*) from Table group by Researcher_Name
                Value = new String[TotalColms - 2 + 1];
            } else {
                Value = new String[TotalColms - 1];
            }
            strToken = nextLine.split("~");
            Key = strToken[0];

            int startPtr;
            if (isRelationalFeatures == true) {
                startPtr = 2;
            } else {
                startPtr = 1;
            }
            for (int i = startPtr; i < TotalColms; i++) {

                try {
                    Value[i - startPtr] = strToken[i];

                } catch (Exception e) {
                    System.out.println(strToken[i]);
                    System.out.print("RuntimeException: ");
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
            }
            // If key found then update value
            if (ResearcherFeature.containsKey(Key)) {
                String[] Buffer = ResearcherFeature.get(Key);
                ResearcherFeature.remove(Key);

                // Ignore 2nd node
                for (int i = 1; i < TotalColms - 2; i++) {
                    // convert into double and add Value + Buffer

                    try {
                        double PreVal = Double.parseDouble(Buffer[i]);
                        double CurVal = Double.parseDouble(Value[i]);
                        Value[i] = new Double(df.format(PreVal + CurVal)).toString();
                    } catch (Exception e) {
                        System.out.println(Path);
                        throw new IllegalAccessException("Line 101: Program.java");
                    }
                }
                if (isRelationalFeatures == true) {
                    Value[Value.length - 1] = String.valueOf(Integer.parseInt(Buffer[Buffer.length - 1]) + 1);
                }
                ResearcherFeature.put(Key, Value);

            } else {
                if (isRelationalFeatures == true) {
                    Value[Value.length - 1] = "1";
                }
                ResearcherFeature.put(Key, Value);
            }
            nextLine = reader.readLine();
            count++;
        }
        // System.out.println("Entries Count = " + count);

        reader.close();
        return ResearcherFeature;
    }

    private static void WriteMaptoFile(String Path, int Year, boolean Print) throws IOException {

        File NF = new File(Path);
        if (NF.exists()) {
            NF.delete();
        }

        PrintWriter Writer = new PrintWriter(new BufferedWriter(new FileWriter(Path, true)));
        Writer.println("Researcher_Name~CarrerTime~LastRestTime~PublicationRate~InterPublicationsInterval~Katz0.25~Katz0.5~Katz0.75~"
                + "Publications~PCN~PJaccard~PPA~PCosine~PAdar~PCC~Collaboration~CCN~CJaccard~CPA~CCosine~CAdar~CCC~TargetPublication~TargetCollaboration");

        // Fuse Files
        int count = 0;
        for (Map.Entry<String, String[]> entry : ResearcherPRF.entrySet()) {
            String RESEARCHER_NAME = entry.getKey();

            // Skipping elements which are not found in both HashMaps
            if (ResearcherNF.containsKey(RESEARCHER_NAME)
                    && ResearcherCRF.containsKey(RESEARCHER_NAME)
                    && ResearcherLabels.containsKey(RESEARCHER_NAME)
                    && TargetLabels.containsKey(RESEARCHER_NAME)) {
                count++;
                String[] FEATURES1 = entry.getValue();
                String[] FEATURES2 = ResearcherNF.get(RESEARCHER_NAME);
                String[] FEATURES3 = ResearcherCRF.get(RESEARCHER_NAME);
                String[] FEATURES4 = ResearcherLabels.get(RESEARCHER_NAME);
                String[] PREDICTIONTARGETS = TargetLabels.get(RESEARCHER_NAME);

                Writer.print(RESEARCHER_NAME + "~");
                if (Print == true) {
                    System.out.print(RESEARCHER_NAME + "~");
                }

                for (int j = 0; j < FEATURES2.length - 1; j++) {
                    // Writer.print(FEATURES2[j]);
                    double[] Katz;
                    if (j == FEATURES2.length - 3) {
                        String delims = "[*]+";
                        String[] token = FEATURES2[j].split(delims);
                        Katz = computerKatz(token);
                        if (Print == true) {
                            System.out.print(Katz[0] + "~" + Katz[1] + "~" + Katz[2]);
                        }
                        Writer.print(Katz[0] + "~" + Katz[1] + "~" + Katz[2]);
                    } else if (j == 0 || j == 1) {
                        if (Print == true) {
                            System.out.print(Year - Integer.parseInt(FEATURES2[j]) + "~");
                        }
                        Writer.print(Year - Integer.parseInt(FEATURES2[j]) + "~");
                    } else {
                        if (Print == true) {
                            System.out.print(FEATURES2[j] + "~");
                        }
                        Writer.print(FEATURES2[j] + "~");
                    }
                }
                // Current Publications count
                if (Print == true) {
                    System.out.print(FEATURES4[0] + "~");
                }
                Writer.print(FEATURES4[0] + "~");
                // Skipping Names
                for (int j = 2; j < FEATURES1.length - 1; j++) {
                    double Avg = Double.parseDouble(FEATURES1[j]) / Double.parseDouble(FEATURES1[FEATURES1.length - 1]);

                    // System.out.print(FEATURES1[j] + "~");
                    if (Print == true) {
                        System.out.print(Avg + "~");
                    }
                    Writer.print(Avg + "~");
                }
                // Compute Clustering Coefficient
                double PublicationCC = -1;
                if (Double.parseDouble(FEATURES1[FEATURES1.length - 1]) > 1) {
                    PublicationCC = 2 * Double.parseDouble(FEATURES1[2])
                            / (Double.parseDouble(FEATURES1[FEATURES1.length - 1])
                            * Double.parseDouble(FEATURES1[FEATURES1.length - 1]) - 1);
                }

                if (Print == true) {
                    System.out.print(PublicationCC + "~");
                }
                Writer.print(PublicationCC + "~");

                // Current Collaborations count
                if (Print == true) {
                    System.out.print(FEATURES4[1] + "~");
                }
                Writer.print(FEATURES4[1] + "~");

                // Skipping Names
                for (int j = 2; j < FEATURES3.length - 1; j++) {
                    double Avg = Double.parseDouble(FEATURES3[j]) / Double.parseDouble(FEATURES3[FEATURES3.length - 1]);

                    if (j == 5 && Avg > 1) {
                        int d = 10;
                    }

                    if (Print == true) {
                        System.out.print(Avg + "~");
                    }
                    Writer.print(Avg + "~");

                }
                double CollaborationCC = -1;
                // -1 -> undefined
                if (Integer.parseInt(FEATURES3[FEATURES3.length - 1]) > 1) {
                    CollaborationCC = 2 * Double.parseDouble(FEATURES3[2])
                            / (Double.parseDouble(FEATURES3[FEATURES3.length - 1])
                            * Double.parseDouble(FEATURES3[FEATURES3.length - 1]) - 1);
                }

                if (Print == true) {
                    System.out.print(CollaborationCC);
                }

                Writer.print(CollaborationCC + "~");
                if (Print == true) {
                    System.out.print(CollaborationCC + "~");
                }
                //////////////////////////////////
                // Prediction Targets
                Writer.print(PREDICTIONTARGETS[0] + "~");
                if (Print == true) {
                    System.out.print(PREDICTIONTARGETS[0] + "~");
                }
                Writer.print(PREDICTIONTARGETS[1]);
                if (Print == true) {
                    System.out.print(PREDICTIONTARGETS[1]);
                }
                //////////////////////////////////
                Writer.println();
            }
        }
        Writer.close();
    }

    public static void main(String[] args) throws FileNotFoundException, IOException, IllegalAccessException {
        int RelationAttributeCount = 9;
        int NodeAttributeCount = 8;
        int LabelAttributeCount = 3;
        String Base = "/home/ahsan/Data/";
        String Dest = "/home/ahsan/Data/COMBINE/";
        String NodeBase = "/home/ahsan/Data/NF/";
        String RelBase = "/home/ahsan/Data/RF/";
        String LabelBase = "/home/ahsan/Data/Labels/";

        LoadFiles(Base);
        int count = 0;
        for (Map.Entry<Integer, LinkedList<String>> entry : FileHashMap.entrySet()) {

            Integer Year = entry.getKey();
            LinkedList<String> Path = entry.getValue();

            // Prediction Year

            Integer WindowYear = Year + 5;
            LinkedList<String> Path2 = FileHashMap.get(WindowYear);
            // System.out.println(WindowYear);
            if (Path2 != null) {
                TargetLabels = LoadFCSV(LabelBase + Path2.get(3), LabelAttributeCount, false, false);
            } else {
                continue;
            }

            ResearcherNF = LoadFCSV(NodeBase + Path.get(0), NodeAttributeCount, false, false);
            ResearcherPRF = LoadFCSV(RelBase + Path.get(1), RelationAttributeCount, true, false);
            ResearcherCRF = LoadFCSV(RelBase + Path.get(2), RelationAttributeCount, true, false);
            ResearcherLabels = LoadFCSV(LabelBase + Path.get(3), LabelAttributeCount, false, false);

            System.out.println(count + " : " + Year);
            WriteMaptoFile(Dest + "COMBINE-FEATURES-" + Year + ".csv", Year, false);
            CombineFiles(Dest);
            // System.out.println(count);
            count++;
        }
    }
    static HashMap<Integer, LinkedList<String>> FileHashMap = new HashMap<Integer, LinkedList<String>>();

    private static void LoadFiles(String Base) {

        File Path = new File(Base + "NF/");
        // Directory path here
        String DirectoryPath = Path.getPath();
        String file;
        File folder = new File(DirectoryPath);
        File[] listOfFiles = folder.listFiles();

        int count = 0;
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                file = listOfFiles[i].getName();
                LinkedList<String> L = new LinkedList<String>();
                L.add(file);
                count++;
                // System.out.println(file);
                FileHashMap.put(Integer.parseInt(file.substring(file.length() - 8, file.length() - 4)), L);
            }
        }
        System.out.println("Total files added " + count);
        Path = new File(Base + "RF/");
        // Directory path here
        DirectoryPath = Path.getPath();

        folder = new File(DirectoryPath);
        listOfFiles = folder.listFiles();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                file = listOfFiles[i].getName();
                // LinkedList<String> L = new LinkedList<String>();
                // L.add(file);
                int Y = Integer.parseInt(file.substring(file.length() - 8, file.length() - 4));
                if (FileHashMap.containsKey(Y)) {
                    LinkedList<String> x = FileHashMap.get(Y);
                    FileHashMap.remove(Y);
                    x.add(file);
                    FileHashMap.put(Y, x);
                } else {
                    throw new IllegalStateException("Same year data must exit for both directories");
                }
                // FileHashMap.put(Integer.parseInt(file.substring(file.length() - 8, file.length() - 4)), L);
                // System.out.println(file);
            }
        }

        Path = new File(Base + "Labels/");
        // Directory path here
        DirectoryPath = Path.getPath();

        folder = new File(DirectoryPath);
        listOfFiles = folder.listFiles();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                file = listOfFiles[i].getName();
                // LinkedList<String> L = new LinkedList<String>();
                // L.add(file);
                int Y = Integer.parseInt(file.substring(file.length() - 8, file.length() - 4));
                if (FileHashMap.containsKey(Y)) {
                    LinkedList<String> x = FileHashMap.get(Y);
                    FileHashMap.remove(Y);
                    x.add(file);
                    FileHashMap.put(Y, x);
                } else {
                    throw new IllegalStateException("Same year data must exit for both directories");
                }
                // FileHashMap.put(Integer.parseInt(file.substring(file.length() - 8, file.length() - 4)), L);
                // System.out.println(file);
            }
        }
    }

    private static double[] computerKatz(String[] token) {
        double[] Katz = new double[3];
        double beta = 0.25;
        int i = 0;
        for (String str : token) {
            Katz[0] += Integer.parseInt(str) * Math.pow(beta, i);
            i++;
        }
        beta = 0.5;
        i = 0;
        for (String str : token) {
            Katz[1] += Integer.parseInt(str) * Math.pow(beta, i);
            i++;
        }
        beta = 0.75;
        i = 0;
        for (String str : token) {
            Katz[2] += Integer.parseInt(str) * Math.pow(beta, i);
            i++;
        }
        return Katz;
    }

    private static void CombineFiles(String DirectoryPath) throws IOException {

        String Name = "Merged.csv";
        File F = new File(DirectoryPath + Name);
        if (F.exists()) {
            F.delete();
        }
        PrintWriter Writer = new PrintWriter(new BufferedWriter(new FileWriter(DirectoryPath + "Merged.csv", true)));
        File folder = new File(DirectoryPath);
        File[] listOfFiles = folder.listFiles();
        String files;
        boolean FirstFile = true;
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                files = listOfFiles[i].getName();

                /**
                 * If you want to list only .TXT files.
                 */
                if (files.endsWith(".csv") || files.endsWith(".CSV")) {
                    if (files.equals(Name)) {
                        continue;
                    }

                    BufferedReader reader = new BufferedReader(new FileReader(DirectoryPath + files));
                    // System.out.println("Reading " + DirectoryPath + files);
                    String nextLine = reader.readLine();

                    // Last element will take count of values for averages
                    boolean header = true;
                    int count = 0;
                    while (nextLine != null) {
                        // Skipping header of file
                        if (header == true && FirstFile == true) {
                            header = false;
                            FirstFile = false;
                            // Write in single
                            Writer.println(nextLine);
                            nextLine = reader.readLine();
                        } else {
                            // Skipping header of each file
                            if (header == true) {
                                nextLine = reader.readLine();
                                header = false;
                            } else {
                                // Write in single
                                Writer.println(nextLine);
                                nextLine = reader.readLine();
                            }

                        }
                    }
                }
            }
        }
        Writer.close();
    }
}
